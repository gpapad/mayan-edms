# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# Przemysław Bodio <przemyslawbodio.dev@gmail.com>, 2024
# mic <winterfall24@gmail.com>, 2024
# tomkolp, 2024
# Roberto Rosario, 2024
# Wojciech Warczakowski <w.warczakowski@gmail.com>, 2024
# 
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-07 07:29+0000\n"
"PO-Revision-Date: 2024-05-07 07:30+0000\n"
"Last-Translator: Wojciech Warczakowski <w.warczakowski@gmail.com>, 2024\n"
"Language-Team: Polish (https://app.transifex.com/rosarior/teams/13584/pl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pl\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);\n"

#: apps.py:32 events.py:11 links.py:52 links.py:93 permissions.py:6
#: settings.py:8 views/event_views.py:29
msgid "Events"
msgstr "Zdarzenia"

#: apps.py:86 apps.py:121
msgid "Date and time"
msgstr "Data i godzina"

#: apps.py:90 apps.py:124 serializers.py:72
msgid "Actor"
msgstr "Czynnik"

#: apps.py:94 apps.py:129
msgid "Event"
msgstr "Zdarzenie"

#: apps.py:98 apps.py:134 serializers.py:78
msgid "Target"
msgstr "Cel"

#: apps.py:102 apps.py:138
msgid "Action object"
msgstr "Obiekt akcji"

#: apps.py:109 forms.py:10 forms.py:71
msgid "Namespace"
msgstr "Przestrzeń nazw"

#: apps.py:114 forms.py:16 forms.py:77 serializers.py:21 serializers.py:46
msgid "Label"
msgstr "Etykieta"

#: apps.py:143
msgid "Seen"
msgstr "Widziane"

#: apps.py:150
msgid "Object"
msgstr "Obiekt"

#: apps.py:155 models.py:48 models.py:109
msgid "Event type"
msgstr "Typ zdarzenia"

#: classes.py:90
msgid "Event list export to CSV"
msgstr ""

#: classes.py:119
msgid "Events exported."
msgstr "Zdarzenia wyeksportowane."

#: classes.py:121
#, python-format
msgid ""
"The event list has been exported and is available for download using the "
"link: %(download_url)s or from the downloads area (%(download_list_url)s)."
msgstr ""

#: events.py:15
msgid "Events cleared"
msgstr "Zdarzenia wyczyszczone"

#: events.py:18
msgid "Events exported"
msgstr "Zdarzenia wyeksportowane"

#: forms.py:22 forms.py:83
msgid "Subscription"
msgstr "Subskrypcja"

#: forms.py:24 forms.py:85
msgid "No"
msgstr "Nie"

#: forms.py:25 forms.py:86
msgid "Subscribed"
msgstr "Subskrybowany"

#: html_widgets.py:23
msgid "System"
msgstr "System"

#: links.py:55 links.py:100 views/clear_views.py:65
msgid "Clear events"
msgstr ""

#: links.py:59 links.py:107 views/export_views.py:64
msgid "Export events"
msgstr ""

#: links.py:65 models.py:55 views/subscription_views.py:54
msgid "Event subscriptions"
msgstr "Subskrypcje zdarzeń"

#: links.py:71 models.py:117 views/subscription_views.py:169
msgid "Object event subscriptions"
msgstr "Subskrypcje zdarzeń obiektowych"

#: links.py:83
msgid "Mark as seen"
msgstr "Oznacz jako widziane"

#: links.py:87
msgid "Mark all as seen"
msgstr "Oznacz wszystkie jako widziane"

#: links.py:114
msgid "Subscriptions"
msgstr "Subskrypcje"

#: literals.py:14
#, python-format
msgid "Unknown or obsolete event type: %s"
msgstr "Nieznany lub nieaktualny typ zdarzenia: %s"

#: models.py:25 serializers.py:24 serializers.py:49
msgid "Name"
msgstr "Nazwa"

#: models.py:29
msgid "Stored event type"
msgstr "Przechowywany typ zdarzenia"

#: models.py:30
msgid "Stored event types"
msgstr "Przechowywane typy zdarzeń"

#: models.py:44 models.py:72 models.py:105 serializers.py:103
msgid "User"
msgstr "Użytkownik"

#: models.py:54
msgid "Event subscription"
msgstr "Subskrypcja zdarzeń"

#: models.py:76 serializers.py:100
msgid "Action"
msgstr "Akcja"

#: models.py:79
msgid "Read"
msgstr "Przeczytane"

#: models.py:86
msgid "Notification"
msgstr "Powiadomienie"

#: models.py:87 views/notification_views.py:33
msgid "Notifications"
msgstr "Powiadomienia"

#: models.py:116
msgid "Object event subscription"
msgstr "Subskrypcja zdarzeń obiektowych"

#: permissions.py:10
msgid "Clear the events of an object"
msgstr ""

#: permissions.py:13
msgid "Export the events of an object"
msgstr ""

#: permissions.py:16
msgid "View the events of an object"
msgstr ""

#: queues.py:7
msgid "Events fast"
msgstr ""

#: queues.py:10
msgid "Events slow"
msgstr ""

#: queues.py:16
msgid "Commit an event"
msgstr ""

#: queues.py:21
msgid "Clear event querysets"
msgstr ""

#: queues.py:26
msgid "Export event querysets"
msgstr ""

#: serializers.py:27 serializers.py:109
msgid "URL"
msgstr "URL"

#: serializers.py:40
msgid "Event type namespace URL"
msgstr ""

#: serializers.py:43
msgid "ID"
msgstr "ID"

#: serializers.py:75
msgid "Actor content type"
msgstr ""

#: serializers.py:81
msgid "Target content type"
msgstr ""

#: serializers.py:84
msgid "Verb"
msgstr "Słowo"

#: settings.py:15
msgid ""
"Disables asynchronous events mode. All events will be committed in the same "
"process that triggers them. This was the behavior prior to version 4.5."
msgstr ""

#: views/clear_views.py:27
msgid ""
"This action is not reversible. The process will be performed in the "
"background. "
msgstr ""

#: views/clear_views.py:53
msgid "Event list clear task queued successfully."
msgstr ""

#: views/clear_views.py:85
#, python-format
msgid "Clear events of: %s"
msgstr ""

#: views/clear_views.py:109
#, python-format
msgid "Clear events of type: %s"
msgstr ""

#: views/event_views.py:25
msgid "Events track actions that have been performed on, to, or with objects."
msgstr ""

#: views/event_views.py:28
msgid "There are no events"
msgstr ""

#: views/event_views.py:49
msgid "There are no events for this object"
msgstr "Brak zdarzeń dla tego obiektu"

#: views/event_views.py:51
#, python-format
msgid "Events for: %s"
msgstr "Zdarzenia dla: %s"

#: views/event_views.py:67
msgid "There are no events of this type"
msgstr ""

#: views/event_views.py:69
#, python-format
msgid "Events of type: %s"
msgstr "Zdarzenia typu: %s"

#: views/export_views.py:28
msgid ""
"The process will be performed in the background. The exported events will be"
" available in the downloads area."
msgstr ""
"Proces zostanie wykonany w tle. Wyeksportowane zdarzenia będą dostępne w "
"obszarze pobierania."

#: views/export_views.py:51
msgid "Event list export task queued successfully."
msgstr ""

#: views/export_views.py:84
#, python-format
msgid "Export events of: %s"
msgstr ""

#: views/export_views.py:102
#, python-format
msgid "Export events of type: %s"
msgstr ""

#: views/notification_views.py:29
msgid "Subscribe to global or object events to receive notifications."
msgstr ""
"Subskrybuj globalne lub obiektowe zdarzenia, aby otrzymywać powiadomienia."

#: views/notification_views.py:32
msgid "There are no notifications"
msgstr "Brak powiadomień"

#: views/notification_views.py:45
msgid "Mark the selected notification as read?"
msgstr "Oznaczyć wybrane powiadomienie jako przeczytane?"

#: views/notification_views.py:60
msgid "Notification marked as read."
msgstr ""

#: views/notification_views.py:72
msgid "Mark all notification as read?"
msgstr "Oznaczyć wszystkie powiadomienia jako przeczytane?"

#: views/notification_views.py:79
msgid "All notifications marked as read."
msgstr "Wszystkie powiadomienia oznaczone jako przeczytane."

#: views/subscription_views.py:38
#, python-format
msgid "Error updating event subscription; %s"
msgstr "Błąd podczas aktualizacji subskrypcji zdarzenia; %s"

#: views/subscription_views.py:43
msgid "Event subscriptions updated successfully"
msgstr "Subskrypcje zdarzeń zostały zaktualizowane pomyślnie"

#: views/subscription_views.py:117
#, python-format
msgid "Error updating object event subscription; %s"
msgstr "Błąd podczas aktualizowania subskrypcji zdarzenia obiektowego; %s"

#: views/subscription_views.py:123
msgid "Object event subscriptions updated successfully"
msgstr "Subskrypcje zdarzeń obiektowych zostały pomyślnie zaktualizowane"

#: views/subscription_views.py:134
#, python-format
msgid "Event subscriptions for: %s"
msgstr "Subskrypcje zdarzeń dla: %s"

#: views/subscription_views.py:164
msgid ""
"Subscribe to the events of an object to receive notifications when those "
"events occur."
msgstr ""

#: views/subscription_views.py:167
msgid "There are no object event subscriptions"
msgstr ""
